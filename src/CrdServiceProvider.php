<?php

namespace Liv\Crd;

use Liv\Crd\Console\MakeBarang;
use Liv\Crd\Console\MakeLayout;
use Liv\Crd\Console\MakeNota;
use Liv\Crd\Console\MakePelanggan;
use Liv\Crd\Console\MakeSp;
use Illuminate\Support\ServiceProvider;

class CrdServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands(
            MakeBarang::class,
            MakeNota::class,
            MakePelanggan::class,
            MakeSp::class,
            MakeLayout::class
        );
    }
}
