<?php

namespace Liv\Crd\Console;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Touhidurabir\StubGenerator\Facades\StubGenerator;

class MakeNota extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:nota {nis}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crud nota';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {
        $nis = $this->argument('nis');

        //controller
        StubGenerator::from('vendor/liv/crd/src/Stubs/Nota/controller.stub', true)
            ->to('app/Http/Controllers/', true, true)
            ->as('NotaController')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();


        //model
        StubGenerator::from('vendor/liv/crd/src/Stubs/Nota/model.stub', true)
            ->to('app/Models/', true, true)
            ->as('Nota')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //migration
        StubGenerator::from('vendor/liv/crd/src/Stubs/Nota/migration.stub', true)
            ->to('database/migrations/', true, true)
            ->as($this->getCurrentDateTimeFormatted() . '_create_notas_table')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //show
        StubGenerator::from('vendor/liv/crd/src/Stubs/Nota/show.stub', true)
            ->to('resource/views/nota', true, true)
            ->as('show.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //data
        StubGenerator::from('vendor/liv/crd/src/Stubs/Nota/data.stub', true)
            ->to('resource/views/nota/datapesanan', true, true)
            ->as('data.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();
    }

    public function getCurrentDateTimeFormatted(): string
    {
        return Carbon::now()->format('Y_m_d_His');
    }
}
