<?php

namespace Liv\Crd\Console;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Touhidurabir\StubGenerator\Facades\StubGenerator;

class MakeBarang extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:barang {nis}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crud Barang';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {
        $nis = $this->argument('nis');

        //controller
        StubGenerator::from('vendor/liv/crd/src/Stubs/Barang/controller.stub', true)
            ->to('app/Http/Controllers/', true, true)
            ->as('BarangController')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();


        //model
        StubGenerator::from('vendor/liv/crd/src/Stubs/Barang/model.stub', true)
            ->to('app/Models/', true, true)
            ->as('Barang')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //migration
        StubGenerator::from('vendor/liv/crd/src/Stubs/Barang/migration.stub', true)
            ->to('database/migrations/', true, true)
            ->as($this->getCurrentDateTimeFormatted() . '_create_barangs_table')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //index
        StubGenerator::from('vendor/liv/crd/src/Stubs/Barang/index.stub', true)
            ->to('resource/views/barang', true, true)
            ->as('index.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //edit
        StubGenerator::from('vendor/liv/crd/src/Stubs/Barang/edit.stub', true)
            ->to('resource/views/barang', true, true)
            ->as('edit.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //create
        StubGenerator::from('vendor/liv/crd/src/Stubs/Barang/create.stub', true)
            ->to('resource/views/barang', true, true)
            ->as('create.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();
    }

    public function getCurrentDateTimeFormatted(): string
    {
        return Carbon::now()->format('Y_m_d_His');
    }
}
