<?php

namespace Liv\Crd\Console;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Touhidurabir\StubGenerator\Facades\StubGenerator;

class MakePelanggan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:pelanggan {nis}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crud pelanggan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {
        $nis = $this->argument('nis');

        //controller
        StubGenerator::from('vendor/liv/crd/src/Stubs/Pelanggan/controller.stub', true)
            ->to('app/Http/Controllers/', true, true)
            ->as('PelangganController')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();


        //model
        StubGenerator::from('vendor/liv/crd/src/Stubs/Pelanggan/model.stub', true)
            ->to('app/Models/', true, true)
            ->as('Pelanggan')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //migration
        StubGenerator::from('vendor/liv/crd/src/Stubs/Pelanggan/migration.stub', true)
            ->to('database/migrations/', true, true)
            ->as($this->getCurrentDateTimeFormatted() . '_create_pelanggans_table')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //index
        StubGenerator::from('vendor/liv/crd/src/Stubs/Pelanggan/index.stub', true)
            ->to('resource/views/pelanggan', true, true)
            ->as('index.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //edit
        StubGenerator::from('vendor/liv/crd/src/Stubs/Pelanggan/edit.stub', true)
            ->to('resource/views/pelanggan', true, true)
            ->as('edit.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //create
        StubGenerator::from('vendor/liv/crd/src/Stubs/Pelanggan/create.stub', true)
            ->to('resource/views/pelanggan', true, true)
            ->as('create.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();
    }

    public function getCurrentDateTimeFormatted(): string
    {
        return Carbon::now()->format('Y_m_d_His');
    }
}
