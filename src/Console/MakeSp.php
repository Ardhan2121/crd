<?php

namespace Liv\Crd\Console;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Touhidurabir\StubGenerator\Facades\StubGenerator;

class MakeSp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:sp {nis}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crud Sp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {
        $nis = $this->argument('nis');

        //controller
        StubGenerator::from('vendor/liv/crd/src/Stubs/Sp/controller.stub', true)
            ->to('app/Http/Controllers/', true, true)
            ->as('TransaksiController')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();


        //model
        StubGenerator::from('vendor/liv/crd/src/Stubs/Sp/model.stub', true)
            ->to('app/Models/', true, true)
            ->as('Sp')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //migration
        StubGenerator::from('vendor/liv/crd/src/Stubs/Sp/migration.stub', true)
            ->to('database/migrations/', true, true)
            ->as($this->getCurrentDateTimeFormatted() . '_create_sps_table')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //pivot
        StubGenerator::from('vendor/liv/crd/src/Stubs/Sp/migration.stub', true)
            ->to('database/migrations/', true, true)
            ->as($this->getCurrentDateTimeFormatted() . '_create_detilpesan_table')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();

        //create
        StubGenerator::from('vendor/liv/crd/src/Stubs/Sp/create.stub', true)
            ->to('resource/views/sp', true, true)
            ->as('create.blade')
            ->withReplacers([
                'nis'    => $nis,
            ])
            ->save();
    }

    public function getCurrentDateTimeFormatted(): string
    {
        return Carbon::now()->format('Y_m_d_His');
    }
}
