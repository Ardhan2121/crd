<?php

namespace Liv\Crd\Console;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Touhidurabir\StubGenerator\Facades\StubGenerator;

class MakeLayout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:layout {nis}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crud Layout';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {
        $nis = $this->argument('nis');

        //layout
        StubGenerator::from('vendor/liv/crd/src/Stubs/Layout/layout.stub', true)
            ->to('resources/views/', true, true)
            ->as('layout.blade')
            ->save();

        StubGenerator::from('vendor/liv/crd/src/Stubs/Layout/sidebar.stub', true)
            ->to('resources/views/components', true, true)
            ->as('sidebar.blade')
            ->save();

        StubGenerator::from('vendor/liv/crd/src/Stubs/Layout/header.stub', true)
            ->to('resources/views/components', true, true)
            ->as('header.blade')
            ->save();

        StubGenerator::from('vendor/liv/crd/src/Stubs/Layout/footer.stub', true)
            ->to('resources/views/components', true, true)
            ->as('footer.blade')
            ->save();

    }

    public function getCurrentDateTimeFormatted(): string
    {
        return Carbon::now()->format('Y_m_d_His');
    }
}
